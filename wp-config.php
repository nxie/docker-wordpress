<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', getenv('DB_NAME'));

/** MySQL database username */
define('DB_USER', getenv('DB_USER'));

/** MySQL database password */
define('DB_PASSWORD', getenv('DB_PASS'));

/** MySQL hostname */
define('DB_HOST', getenv('DB_HOST'));

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('DISALLOW_FILE_EDIT', true);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b%Y|LL|fGfyNLH oZ}u7w|^3K]Filwr +w%f#R5D0 !=L8YPD.vbqbZW c}/VJoC');
define('SECURE_AUTH_KEY',  'WiIIJ=;a:fD7V`6Q!3_ovmaT@$:5rT1i4-Iqc6SFPiUdZhbhruIia`vME-14$S77');
define('LOGGED_IN_KEY',    'US-=m J0+z0@Ga@m]@Ilw,U+.n+TV/$C,LR=5ZJcS{4l-q@9$B$i`Ae8n:>v|;;5');
define('NONCE_KEY',        'xXl5 m[UNpW.}Rk@ah*-:LNJ!y}Vk;K#97f&[b]s7$u1-GX8r.-^=tA-n3ARj!V?');
define('AUTH_SALT',        '*JZ *C:JKdjOGJOTQSVt7Ap1:D+~Z+xQa#>J^%_wi_1|L~9=-;n#6a*4s1H|v$B`');
define('SECURE_AUTH_SALT', '6|7,<$9lRi$t[0iG(o7D7w5wFZs1JP+Xo+[U<)PM6J.113r-M;x!hm;ZN}uinp;Y');
define('LOGGED_IN_SALT',   '-Jh.lc= *[urT^u(15T 3-i~L/-LEx+V%D %5B6sum3cWnHjO6-/(Y.u+Q=bjLVa');
define('NONCE_SALT',       'r|i|-ojY|v0Z|lkO9&9;7A+(:$C?CLJ-n0<9*PFZ4)-@$>>u:nZz,6Mz{=acgw2I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
define('WP_CACHE', true);

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
